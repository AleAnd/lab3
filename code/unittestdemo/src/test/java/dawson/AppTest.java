package dawson;

import static org.junit.Assert.*;


import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void echo(){
        App app = new App();
        assertEquals("Testing if echo returns its input",app.echo(5),5);
    }

    @Test
    public void oneMore(){
        App app = new App();
        assertEquals("Testing if it adds one to the input it is given", app.oneMore(8),9);
    }
}
